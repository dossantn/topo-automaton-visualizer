import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.widgets
import matplotlib.colors as mcolors
import argparse
import string
#import random

def close_on_close(event):
    plt.close('all')

#cell definition: (x0, y0, x_len, y_len, SNR, E, is_secondary)
#colour definition: something matplotlib interprets as a colour for each of the clusters

def seed_tag(orderer, cell):
    return np.uint32((cell & 0xFFF) | ((orderer & 0xFFF) << 12) | 0xFF000000)

def splitter_tag(primary, shared, orderer, cell):
    return np.uint32((cell & 0xFFF) | ((orderer & 0xFFF) << 12) | (bool(primary) * 0x80000000) | ((not shared) * 0x40000000)) | 0x3F000000

invalid_tag = 0
terminal_tag = 1
growing_tag = 2

seed_threshold = 4
grow_threshold = 2
terminal_threshold = 0

def final_tag(first_index, second_weight = 0, second_index = 0):
    weight_pattern = int(127 * second_weight)
    if weight_pattern == 0 and second_weight > 0:
        weight_pattern = 1
    return 0x80000000 | (first_index & 0xFFF) | ((second_index & 0xFFF) << 19) | ((weight_pattern & 0x7F) << 12)

def touch_x(r1, r2):
    return (r2[0] >= r1[0] and r2[0] <= r1[0] + r1[2]) or (r1[0] >= r2[0] and r1[0] <= r2[0] + r2[2])

def touch_y(r1, r2):
    return (r2[1] >= r1[1] and r2[1] <= r1[1] + r1[3]) or (r1[1] >= r2[1] and r1[1] <= r2[1] + r2[3])

def check_cell_neighbourhood(r1, r2):
    if ((r1[0] == r2[0] + r2[2] or r1[0] + r1[2] == r2[0]) and
        (r1[1] == r2[1] + r2[3] or r1[1] + r1[3] == r2[1])      ):
        return False
    return touch_x(r1, r2) and touch_y(r1, r2)

class Simul:
    def grow(self):
    
        merge_table = {}
        state = np.zeros(self.num_cells, dtype = np.uint32)
        
        ########### Classify cells
        
        sorter = np.flip(np.argsort(self.SNRs, kind='stable'))
        #If the sort is stable, equal SNR will be sorted by cell index
        #Since we then flip, this means that greater indices compare greater,
        #which is exactly what we want!
        
        #clusterlist = random.shuffle([i in range [0, 0xFFF]])
        #On a GPU, cluster order is essentially random!
        
        for i in range(0, len(sorter)):
            if self.SNRs[sorter[i]] < seed_threshold:
                break
            merge_table[sorter[i]] = self.num_clusters; #clusterlist[i]
            self.num_clusters += 1
            state[sorter[i]] = seed_tag(len(sorter) - i, sorter[i])
                
        state += np.array( (self.SNRs <= seed_threshold) * (self.SNRs > grow_threshold) * growing_tag + 
                           (self.SNRs <= grow_threshold) * (self.SNRs > terminal_threshold) * terminal_tag,
                           dtype = np.uint32)
        
        self.grow_steps += [(state.copy(), merge_table.copy())]
        
        loop_condition = True
        
        other_state = state.copy()
        
        ########### Grow clusters
        
        while loop_condition:
            loop_condition = False
            
            for i in range(0, self.num_cells):
                if state[i] > growing_tag:
                    new_tag = 0
                    for j in range (1, self.neighs[i][0] + 1):
                        neigh_tag = state[self.neighs[i][j]]
                        if neigh_tag <= growing_tag:
                            continue
                        new_tag = max(new_tag, neigh_tag - (neigh_tag > growing_tag) * 0x01000000)
                        old_cluster = (state[i] & 0xFFF)
                        new_cluster = (neigh_tag  & 0xFFF)
                        old_real_cluster = merge_table[old_cluster]
                        new_real_cluster = merge_table[new_cluster]
                        if old_cluster != new_cluster and old_real_cluster != new_real_cluster:
                            loop_condition = True
                            final_cluster = min(old_real_cluster, new_real_cluster)
                            merge_table[old_cluster] = final_cluster
                            merge_table[new_cluster] = final_cluster
                    if new_tag > state[i]:
                        other_state[i] = new_tag
                        loop_condition = True
                elif state[i] == growing_tag:
                    new_tag = 0
                    for j in range (1, self.neighs[i][0] + 1):
                        neigh_tag = state[self.neighs[i][j]]
                        new_tag = max(new_tag, neigh_tag - (neigh_tag > growing_tag) * 0x01000000)
                    if new_tag > growing_tag:
                        other_state[i] = new_tag
                        loop_condition = True
                else:
                    continue
            
            if not loop_condition:
                break
            
            np.copyto(state, other_state)
            
            self.grow_steps += [(state.copy(), merge_table.copy())]
       
        ########### Get terminal cells
        
        for i in range(0, self.num_cells):
            if state[i] != terminal_tag:
                continue
            new_tag = 0
            for j in range (1, self.neighs[i][0] + 1):
                new_tag = max(new_tag, other_state[self.neighs[i][j]])
            if new_tag <= growing_tag:
                continue
            state[i] = new_tag
        
        self.grow_steps += [(state.copy(), merge_table.copy())]
        
        ########### Finalize
        
        for i in range(0, self.num_cells):
            if state[i] <= growing_tag:
                state[i] = invalid_tag
            else:
                state[i] = final_tag(merge_table[state[i] & 0xFFF])
        
        self.grow_final = state.copy()
    
    def split(self):
    
        ########### Build intra-cluster neighbours
        
        new_neighbours = np.zeros((self.num_cells, self.num_max_neighs + 1), dtype = np.uint32)
        
        for i in range(0, self.num_cells):
            this_tag = self.grow_final[i]
            if this_tag >= 0x80000000:
                for j in range(1, self.neighs[i][0] + 1):
                    neigh_cell = self.neighs[i][j]
                    if neigh_cell <= i:
                        continue
                    neigh_tag = self.grow_final[neigh_cell]
                    if this_tag & 0xFFF == neigh_tag & 0xFFF:
                        new_neighbours[i][0] += 1
                        new_neighbours[i][new_neighbours[i][0]] = neigh_cell
                        new_neighbours[neigh_cell][0] += 1
                        new_neighbours[neigh_cell][new_neighbours[neigh_cell][0]] = i
        
        #for i in range(0, self.num_cells):
        #    print("{}:\n   {}".format(i, new_neighbours[i]))
        
        ########### Identify local maxima
        
        sorter = np.flip(np.argsort(self.Es, kind='stable'))
        #If the sort is stable, equal E will be sorted by cell index
        #Since we then flip, this means that greater indices compare greater,
        #which is exactly what we want!
        
        orderer = np.zeros(self.num_cells, dtype = np.uint32)
        
        for i in range(0, len(sorter)):
            orderer[sorter[i]] = len(sorter) - i
        
        state = np.zeros(self.num_cells, dtype = np.uint32)
        
        invalidated_clusters = []
        
        index_to_cluster_map = -np.ones(self.num_cells, dtype = np.uint32)
                
        for i in range(0, self.num_cells):
            if self.grow_final[i] >= 0x80000000:
                can_be_maximum = new_neighbours[i][0] > 1
                if can_be_maximum:
                    for j in range(1, new_neighbours[i][0] + 1):
                        if self.Es[new_neighbours[i][j]] >= self.Es[i] and not (not self.secondary[i] and self.secondary[new_neighbours[i][j]]):
                            can_be_maximum = False
                            break
                if can_be_maximum:
                    state[i] = splitter_tag(not self.secondary[i], 0, orderer[i], i)
                    index_to_cluster_map[i] = self.num_clusters
                    self.num_clusters += 1
                    invalidated_clusters.append(self.grow_final[i] & 0xFFF)
            
        for i in range(0, self.num_cells):
            old_tag = self.grow_final[i]
            if state[i] == 0 and old_tag >= 0x80000000:
                if old_tag & 0xFFF in invalidated_clusters:
                    state[i] = (old_tag & 0xFFF) | 0x1000
                else:
                    state[i] = old_tag | 0xFFFFF000
        
        self.split_steps += [(state.copy(), index_to_cluster_map.copy())]
        
        ########### Eliminate secondary local maxima
        
        second_state = np.zeros(self.num_cells, dtype = np.uint32)
        
        for i in range(0, self.num_cells):
            if state[i] & 0xFFFFF000 == 0xFFFFF000:
                #Original clusters
                state[i] = 0
                second_state[i] = 0
            elif state[i] & 0xBF000000 == 0xBF000000:
                #Primary local maxima
                state[i] = 0xFFFFFFFF
                second_state[i] = 0xFFFFFFFF
            elif state[i] >= 0x3F000000:
                #Secondary local maxima
                second_state[i] = state[i]
            else:
                #Whatever happens, we want to propagate
                state[i] = 0
                second_state[i] = 0
        
        self.secondary_elimination_steps += [(state.copy(), second_state.copy())]
        
        loop_condition = True
        
        while loop_condition:
            loop_condition = False
            for i in range(0, self.num_cells):
                left_tag = state[i]
                for j in range(1, self.secondary_left_neighs[i][0] + 1):
                    left_tag = max(left_tag, state[self.secondary_left_neighs[i][j]])
                if left_tag > state[i]:
                    state[i] = left_tag
                    loop_condition = True
                
                right_tag = second_state[i]
                for j in range(1, self.secondary_right_neighs[i][0] + 1):
                    right_tag = max(right_tag, second_state[self.secondary_right_neighs[i][j]])
                if right_tag > second_state[i]:
                    second_state[i] = right_tag
                    loop_condition = True
            
            if not loop_condition:
                break
                
            self.secondary_elimination_steps += [(state.copy(), second_state.copy())]
                
        
        ########### Actual splitting
        
        np.copyto(state, self.split_steps[0][0])
        
        for i in range(0, self.num_cells):
            this_tag = state[i]
            if this_tag >= 0x3F000000 and this_tag < 0xBF000000:
                #Secondary maximum
                if ( (self.secondary_elimination_steps[-1][0][i] & 0xFFF) != (this_tag & 0xFFF) or 
                     (self.secondary_elimination_steps[-1][1][i] & 0xFFF) != (this_tag & 0xFFF)    ):
                    index_to_cluster_map[this_tag & 0xFFF] = -1
                    state[i] = (self.grow_final[i] & 0xFFF) | 0x1000
            if state[i] > 0x1FFF:
                state[i] = state[i] | 0x80000000
                #We no longer distinguish primary and secondary maxima...
        
        
        self.split_steps += [(state.copy(), index_to_cluster_map.copy())]
        
        np.copyto(second_state, state)
        
        loop_condition = True
        
        while loop_condition:
            loop_condition = False
            reset_counter = 0
            for i in range(0, self.num_cells):
                this_tag = state[i]
                
                if this_tag == 0:
                    continue
                
                if this_tag & 0xFFFFF000 == 0xFFFFF000:
                    #Original clusters unchanged
                    continue
                
                
                new_tag = this_tag
                
                for j in range(1, new_neighbours[i][0] + 1):
                    neigh = new_neighbours[i][j]
                    neigh_tag = state[neigh]
                    if neigh_tag <= 0x1FFF or neigh_tag & 0xFFFFF000 == 0xFFFFF000:
                        continue
                    elif not neigh_tag & 0x40000000 and not neigh_tag & 0x80000000 and neigh_tag & 0x3F000000 > 0x1F000000:
                        neigh_tag = (neigh_tag & 0x00FFFFFF) | 0x1F000000  
                    elif neigh_tag & 0x40000000 and this_tag > 0x1FFF and this_tag & 0xC0000000 == 0x40000000:
                        neigh_tag = (neigh_tag - 0x01000000) & 0x7FFFFFFF
                        if (neigh_tag & 0x3F000000) == (this_tag & 0x3F000000) and index_to_cluster_map[neigh_tag & 0xFFF] != index_to_cluster_map[this_tag & 0xFFF]:
                            neigh_tag = (neigh_tag & 0x00FFFFFF) | 0xBF000000
                            reset_counter = max(reset_counter, this_tag & 0x3F000000) 
                    else:
                        neigh_tag = (neigh_tag - 0x01000000) & 0x7FFFFFFF
                    new_tag = max(new_tag, neigh_tag)
                
                second_state[i] = new_tag
                        
            for i in range(0, self.num_cells):
                old_tag = state[i]
                new_tag = second_state[i]
                if new_tag <= 0x1FFF or new_tag & 0xFFFFF000 == 0xFFFFF000:
                    continue
                old_index = index_to_cluster_map[old_tag & 0xFFF]
                new_index = index_to_cluster_map[new_tag & 0xFFF]
                if new_tag & 0x3F000000 < 0x3F000000:
                    if new_tag & 0x3F000000 < reset_counter or old_tag & 0x3F000000 < reset_counter:
                        state[i] = self.split_steps[1][0][i]
                        second_state[i] = state[i]
                        loop_condition = True
                        index_to_cluster_map[i] = -1
                        continue
                    elif not new_tag & 0x80000000:
                        index_to_cluster_map[i] = new_index
                
                if new_tag == old_tag:
                    continue
                
                if old_index == new_index:
                    new_tag = (new_tag & 0xFFF000000) | (orderer[i] << 12) | i
                    if new_tag != old_tag:
                        loop_condition = True
                    state[i] = new_tag
                    second_state[i] = new_tag
                    continue
                
                if old_tag > 0x1FFF and old_tag & 0x40000000 and new_tag & 0xC0000000 == 0x80000000:
                    new_tag = ( 0x80000000 | (old_tag & 0x3F000000) | (orderer[i] << 12) | i ) + 0x01000000
                    min_idx = min(old_index, new_index)
                    max_idx = max(old_index, new_index)
                    index_to_cluster_map[i] = (max_idx << 12) | min_idx
                else:
                    new_tag = (new_tag & 0xFFF000000) | (orderer[i] << 12) | i
                
                loop_condition = True
                
                state[i] = new_tag
                second_state[i] = new_tag
                
            if not loop_condition:
                break
            
            self.split_steps += [(state.copy(), index_to_cluster_map.copy())]
        
        ########### Calculate weights
        
        weights = np.zeros((self.num_clusters, 4))
        
        for i in range(0, self.num_cells):
            if state[i] <= 0x1FFF or not state[i] & 0x40000000:
                continue
            abs_E = abs(self.Es[i])
            weights[index_to_cluster_map[state[i] & 0xFFF]] += [*(self.coords[i] * abs_E), abs_E, self.Es[i]]
        
        
        ########### Finalize
                
        for i in range(0, self.num_cells):
            tag = state[i]
            if tag & 0xFFFFF000 == 0xFFFFF000:
                state[i] = final_tag(tag & 0xFFF)
            elif tag > 0x1FFF:
                if not tag & 0x40000000:
                    #shared
                    two_indices = index_to_cluster_map[tag & 0xFFF]
                    idx_1 = two_indices & 0xFFF
                    idx_2 = (two_indices & 0xFFF000) >> 12
                    delta_1 = self.coords[i] - weights[idx_1][0:2] / weights[idx_1][2]
                    delta_2 = self.coords[i] - weights[idx_2][0:2] / weights[idx_2][2]
                    r_exp = np.linalg.norm(delta_1) - np.linalg.norm(delta_2)
                    r_exp = -10 if r_exp < -10 else 10 if r_exp > 10 else r_exp
                    r     = np.exp( r_exp)
                    r_rev = np.exp(-r_exp)
                                        
                    w     = weights[idx_1][3] / (weights[idx_1][3] + weights[idx_2][3] * r    )
                    w_rev = weights[idx_2][3] / (weights[idx_2][3] + weights[idx_1][3] * r_rev)
                    
                    if w > 0.5:
                        state[i] = final_tag(idx_1, w_rev, idx_2)
                    elif w == 0.5:
                        state[i] = final_tag(max(idx_1, idx_2), 0.5, min(idx_1, idx_2))
                    else:
                        state[i] = final_tag(idx_2, w, idx_1)
                else:
                    state[i] = final_tag(index_to_cluster_map[tag & 0xFFF])
            else:
                state[i] = invalid_tag
        
            
        self.split_final = state.copy()
        
    def render_from_final(self, state):
        for i in range(0, self.num_cells):
            if self.label_options == "Tag":
                self.cell_labels[i].set_text("{:08X}".format(state[i]))
            
            index_1 = (state[i] & 0xFFF) if state[i] & 0x80000000 else None
            index_2 = (state[i] & 0x7FF80000) >> 19 if state[i] > 0x80000FFF else None
            
            self.splitter_rounds[i].set_visible(False)
            self.splitter_shared[i].set_visible(False)
            self.sme_left_rects[i].set_visible(False)
            self.sme_right_rects[i].set_visible(False)
            self.shared_rects[i].set_visible(False)
            
            if index_1 is not None:
                if self.draw_colours:
                    self.rects[i].set_facecolor(self.colormap[index_1])
                    
                if index_2 is not None:
                    if self.draw_colours:
                        self.shared_rects[i].set_visible(True)
                        self.shared_rects[i].set_edgecolor(self.colormap[index_2])
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text("{} | {}".format(self.namemap[index_1], self.namemap[index_2]))
                else:
                    if self.draw_colours:
                        pass
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text(self.namemap[index_1])
                    
            else:
                if self.draw_colours:
                    self.rects[i].set_facecolor(self.invalid_color)
                if self.label_options == "Cluster":
                    self.cell_labels[i].set_text("")
                
    
    def set_step(self, step):
        self.current_step = step
        
        if step == 0:
            for i in range(0, self.num_cells):
                self.splitter_rounds[i].set_visible(False)
                self.splitter_shared[i].set_visible(False)
                self.sme_left_rects[i].set_visible(False)
                self.sme_right_rects[i].set_visible(False)
                self.shared_rects[i].set_visible(False)
                if self.SNRs[i] > seed_threshold:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.seed_color)
                    if self.label_options == "Tag":
                        self.cell_labels[i].set_text("")
                elif self.SNRs[i] > grow_threshold:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.growing_color)
                    if self.label_options == "Tag":
                        self.cell_labels[i].set_text("{:08X}".format(growing_tag))
                elif self.SNRs[i] > terminal_threshold:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.terminal_color)
                    if self.label_options == "Tag":
                        self.cell_labels[i].set_text("{:08X}".format(terminal_tag))
                else:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.invalid_color)
                    if self.label_options == "Tag":
                        self.cell_labels[i].set_text("{:08X}".format(invalid_tag))
                    
        elif step >= self.grow_start and step <= self.grow_end:
            this_state = self.grow_steps[step-self.grow_start][0]
            this_table = self.grow_steps[step-self.grow_start][1]
            for i in range(0, self.num_cells):
                self.splitter_rounds[i].set_visible(False)
                self.splitter_shared[i].set_visible(False)
                self.sme_left_rects[i].set_visible(False)
                self.sme_right_rects[i].set_visible(False)
                self.shared_rects[i].set_visible(False)
                if self.label_options == "Tag":
                    self.cell_labels[i].set_text("{:08X}".format(this_state[i]))
                if this_state[i] > growing_tag:
                    this_index = this_table[this_state[i] & 0xFFF]
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.colormap[this_index])
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text(self.namemap[this_index])
                elif this_state[i] > terminal_tag:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.growing_color)
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text("")
                elif this_state[i] > invalid_tag:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.terminal_color)
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text("")
                else:
                    if self.draw_colours:
                        self.rects[i].set_facecolor(self.invalid_color)
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text("")
                        
                        
        elif step == self.grow_end + 1:
            self.render_from_final(self.grow_final)
                    
        elif step >= self.secondary_elimination_start and step <= self.secondary_elimination_end:
            self.render_from_final(self.grow_final)
            index = step - self.secondary_elimination_start
            this_left = self.secondary_elimination_steps[index][0]
            this_right = self.secondary_elimination_steps[index][1]
            table = self.split_steps[0][1]
            for i in range(0, self.num_cells):
                self.sme_left_rects[i].set_visible(True)
                self.sme_right_rects[i].set_visible(True)
                one = (self.sme_left_rects[i], this_left[i])
                two = (self.sme_right_rects[i], this_right[i])
                for r,tag in [one, two]:
                    if self.draw_colours:
                        if tag == 0xFFFFFFFF:
                            r.set_facecolor("black")
                        elif tag == 0:
                            r.set_facecolor(self.growing_color)
                        else:
                            r.set_facecolor(self.colormap[table[tag & 0xFFF]])
                if self.label_options == "Cluster":
                    self.cell_labels[i].set_text("")
                elif self.label_options == "Tag":
                    self.cell_labels[i].set_text("{:08X}\n---------------\n{:08X}".format(this_right[i], this_left[i]))

            
        elif step == self.split_first or (step >= self.split_restart and step <= self.split_end):
            self.render_from_final(self.grow_final)
            index = step - self.split_restart + 1 if step >= self.split_restart else 0
            this_state = self.split_steps[index][0]
            this_table = self.split_steps[index][1]
            for i in range(0, self.num_cells):
                self.sme_left_rects[i].set_visible(False)
                self.sme_right_rects[i].set_visible(False)
                self.shared_rects[i].set_visible(False)
                tag = this_state[i]
                if self.label_options == "Tag":
                    self.cell_labels[i].set_text("{:08X}".format(tag))
                if tag <= 0x1FFF:
                    self.splitter_rounds[i].set_visible(False)
                    self.splitter_shared[i].set_visible(False)
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text("")
                elif tag >= 0xFFFFF000:
                    self.splitter_shared[i].set_visible(False)
                    self.splitter_rounds[i].set_visible(False)
                    if self.label_options == "Cluster":
                        self.cell_labels[i].set_text(self.namemap[tag & 0xFFF])
                else:
                    if not tag & 0x40000000:
                        #Shared
                        two_indices = this_table[tag & 0xFFF]
                        self.splitter_shared[i].set_visible(True)
                        self.splitter_rounds[i].set_visible(True)
                        self.splitter_rounds[i].set_facecolor(self.colormap[two_indices & 0xFFF])
                        self.splitter_shared[i].set_edgecolor(self.colormap[(two_indices & 0xFFF000) >> 12])
                        if self.label_options == "Cluster":
                            self.cell_labels[i].set_text("{} | {}".format(self.namemap[two_indices & 0xFFF], self.namemap[(two_indices & 0xFFF000) >> 12]))
                    else:
                        self.splitter_shared[i].set_visible(False)
                        self.splitter_rounds[i].set_visible(True)
                        self.splitter_rounds[i].set_facecolor(self.colormap[this_table[tag & 0xFFF]])
                        if self.label_options == "Cluster":
                            self.cell_labels[i].set_text(self.namemap[this_table[tag & 0xFFF]])
                        
        else:
            self.render_from_final(self.split_final)
        
        if self.label_options == "None":
            for lab in self.cell_labels:
                lab.set_visible(False)
            return

        for lab in self.cell_labels:
            lab.set_visible(True)
        
        if self.label_options == "Index":
            for i in range (0, self.num_cells):
                self.cell_labels[i].set_text(str(i))
        elif self.label_options == "SNR":
            for i in range (0, self.num_cells):
                self.cell_labels[i].set_text(str(self.SNRs[i]))
        elif self.label_options == "E":
            for i in range (0, self.num_cells):
                self.cell_labels[i].set_text(str(self.Es[i]))
                
    
    def toggle_option(self, option):
        if option == "Cell Colours":
            self.draw_colours = not self.draw_colours
            if self.draw_colours:
                self.set_step(self.current_step)
                #Update/recalculate the cell colours...
            else:
                for r in self.rects:
                    r.set_facecolor("none")
                #for r in 
        elif option == "Cell Borders":
            self.draw_borders = not self.draw_borders
            for i in range(0, self.num_cells):
                self.sme_left_rects[i].set_edgecolor(self.edge_color if self.draw_borders else "none")
                self.sme_right_rects[i].set_edgecolor(self.edge_color if self.draw_borders else "none")
                if self.highlight_secondaries and self.secondary[i]:
                    self.rects[i].set_edgecolor(self.highlighted_color)
                else:
                    if self.draw_borders:
                        self.rects[i].set_edgecolor(self.edge_color)
                    else:
                        self.rects[i].set_edgecolor("none")
            
        elif option == "Axis":
            self.draw_axis = not self.draw_axis
            if self.draw_axis:
                self.axis.set_axis_on()
            else:
                self.axis.set_axis_off()
        elif option == "Secondaries":
            self.highlight_secondaries = not self.highlight_secondaries
            if self.highlight_secondaries:
                for i in range(0, self.num_cells):
                    if self.secondary[i]:
                        self.rects[i].set_hatch("xxx")
                        self.rects[i].set_edgecolor(self.highlighted_color)
            else:
                for r in self.rects:
                    r.set_hatch("")
                    if self.draw_borders:
                        r.set_edgecolor(self.edge_color)
                    else:
                        r.set_edgecolor("none")
                
            
                
    def set_tag_style(self, option):
        if not self.label_options == option:
            self.label_options = option
            self.set_step(self.current_step)
            #Update/recalculate the labels...
    
    def __init__(self, rectangle_definitions, colormap, namemap):
        self.fig, self.axis = plt.subplots(num="Simulation", figsize=(5,5))
        self.fig.canvas.mpl_connect('close_event', close_on_close)
        self.colormap = colormap
        self.namemap = namemap
        self.SNRs = np.array([p[4] for p in rectangle_definitions], dtype=np.single)
        self.Es = np.array([p[5] for p in rectangle_definitions], dtype=np.single)
        self.coords = np.array([[p[0] + p[2]/2, p[1] + p[3]/2] for p in rectangle_definitions], dtype=np.single)
        self.secondary = np.array([p[6] for p in rectangle_definitions], dtype=bool)
        self.rects = [ matplotlib.patches.Rectangle( (p[0], p[1]), p[2], p[3], linewidth=1,
                                                      edgecolor = 'black', facecolor='white' )
                       for p in rectangle_definitions ]
        self.shared_rects = [ matplotlib.patches.Rectangle( (p[0], p[1]), p[2], p[3], linewidth=1, edgecolor = 'none',
                                                            facecolor='none', linestyle = '', hatch='.o'                  )
                              for p in rectangle_definitions ]
        
        self.sme_left_rects = [ matplotlib.patches.Rectangle( (p[0] + 0.05 * p[2] , p[1] + 0.05 * p[3]), 0.9 * p[2], 0.4 * p[3],
                                                                linewidth=1, edgecolor = 'black', facecolor='white', visible = False  )
                                for p in rectangle_definitions ]
        self.sme_right_rects = [ matplotlib.patches.Rectangle( (p[0] + 0.05 * p[2], p[1] + 0.55 * p[3]), 0.9 * p[2], 0.4 * p[3],
                                                                linewidth=1, edgecolor = 'black', facecolor='white', visible = False )
                                for p in rectangle_definitions ]
        
        self.splitter_rounds = [ matplotlib.patches.FancyBboxPatch( (p[0] + 0.1 * p[2]+ 0.2, p[1] + 0.1 * p[3] + 0.2), p[2] * 0.8 - 0.4, p[3] * 0.8 - 0.4,
                                                                     linewidth=1, edgecolor = 'black', facecolor='none', visible = False,
                                                                     boxstyle=matplotlib.patches.BoxStyle("Round", pad=0.2))
                                 for p in rectangle_definitions ]
        self.splitter_shared = [ matplotlib.patches.FancyBboxPatch( (p[0] + 0.1 * p[2] + 0.2, p[1] + 0.1 * p[3]+ 0.2), p[2] * 0.8 - 0.4, p[3] * 0.8 - 0.4,
                                                                     linewidth=1, edgecolor = 'none', facecolor='none',
                                                                     linestyle='', hatch='.o', visible = False,
                                                                     boxstyle=matplotlib.patches.BoxStyle("Round", pad=0.2)                      )
                                 for p in rectangle_definitions ]
        
        self.num_cells = len(rectangle_definitions)
        
        for sr in self.rects:
            self.axis.add_patch(sr)
            
        for sr in self.shared_rects:
            self.axis.add_patch(sr)
            
        for sr in self.sme_left_rects:
            self.axis.add_patch(sr)
            
        for sr in self.sme_right_rects:
            self.axis.add_patch(sr)
            
        for sr in self.splitter_rounds:
            self.axis.add_patch(sr)
            
        for sr in self.splitter_shared:
            self.axis.add_patch(sr)
            
            
            
        self.cell_labels = [ self.axis.text(p[0] + p[2]/2, p[1] + p[3]/2, "",
                                            ha="center", va="center",
                                            bbox=dict(facecolor='white',
                                                      edgecolor='black',
                                                      boxstyle='round')
                                           )
                             for p in rectangle_definitions ]
                
        self.fig.tight_layout()
        
        self.label_options = "None"
        
        self.draw_axis = True
        self.draw_borders = True
        self.draw_colours = True
        self.highlight_secondaries = False
        
        self.invalid_color = "#f2f2f2"
        self.terminal_color = "#bfbfbf"
        self.growing_color = "#7f7f7f"
        self.seed_color = "#404040"
        self.edge_color = "#000000"
        self.highlighted_color = "#000000"
        
        self.num_max_neighs = 20 #We're 2D...
        
        self.neighs = np.zeros((self.num_cells, self.num_max_neighs + 1), dtype = np.uint32)
        #First is num neighs
        self.secondary_left_neighs = np.zeros((self.num_cells, self.num_max_neighs + 1), dtype = np.uint32)
        self.secondary_right_neighs = np.zeros((self.num_cells, self.num_max_neighs + 1), dtype = np.uint32)
        
        for i in range(0, self.num_cells):
            cell_1 = rectangle_definitions[i]
            for j in range(i + 1, self.num_cells):
                cell_2 = rectangle_definitions[j]
                if check_cell_neighbourhood(cell_1, cell_2):
                    self.neighs[i][0] += 1
                    self.neighs[i][self.neighs[i][0]] = j
                    self.neighs[j][0] += 1
                    self.neighs[j][self.neighs[j][0]] = i
                    if cell_1[1] == cell_2[1] + cell_2[3] or cell_1[1] + cell_1[3] == cell_2[1]:
                        continue
                    if cell_1[0] == cell_2[0] + cell_2[2]:
                        self.secondary_left_neighs[i][0] += 1
                        self.secondary_left_neighs[i][self.secondary_left_neighs[i][0]] = j
                        self.secondary_right_neighs[j][0] += 1
                        self.secondary_right_neighs[j][self.secondary_right_neighs[j][0]] = i
                    elif cell_1[0] + cell_1[2] == cell_2[0]:
                        self.secondary_right_neighs[i][0] += 1
                        self.secondary_right_neighs[i][self.secondary_right_neighs[i][0]] = j
                        self.secondary_left_neighs[j][0] += 1
                        self.secondary_left_neighs[j][self.secondary_left_neighs[j][0]] = i
        
        for i in range(0, self.num_cells):
            print("--------------------------------------")
            print(i)
            print(self.neighs[i])
            print(self.secondary_left_neighs[i])
            print(self.secondary_right_neighs[i])
        
        self.grow_steps = []
        self.secondary_elimination_steps = []
        self.split_steps = []
        
        self.num_clusters = 0
        
        self.grow()
        self.split()
                
        self.max_step_size = len(self.grow_steps) + len(self.secondary_elimination_steps) + len(self.split_steps) + 2
        
        #0 is initial situation
        
        self.grow_start = 1
        
        self.grow_end = len(self.grow_steps)
        
        #grow_end + 1 is post-grow state
        
        self.split_first = self.grow_end + 2
        
        self.secondary_elimination_start = self.grow_end + 3
        self.secondary_elimination_end = self.grow_end + 2 + len(self.secondary_elimination_steps)
        
        self.split_restart = self.secondary_elimination_end + 1
        self.split_end = self.secondary_elimination_end + len(self.split_steps) - 1
        
        self.current_step = -1
        
        self.set_step(0)
        

class Player(FuncAnimation):
    #Adapted from: https://stackoverflow.com/a/46327978
    def __init__(self, simulation, fargs=None, save_count=None, **kwargs):
        self.i = 0
        self.slider_min=0
        self.slider_max=simulation.max_step_size
        self.runs = True
        self.forwards = True
        self.fig = simulation.fig
        self.func = simulation.set_step
        self.simul = simulation
        self.setup()
        FuncAnimation.__init__(self,self.fig, self.update, frames=self.play(),
                               fargs=fargs, save_count=save_count, **kwargs )    
        
    def play(self):
        while self.runs:
            self.i = self.i+self.forwards-(not self.forwards)
            if self.i > self.slider_min and self.i < self.slider_max:
                yield self.i
            else:
                self.stop()
                if self.i < self.slider_min:
                    self.i = self.slider_min
                elif self.i > self.slider_max:
                    self.i = self.slider_max
                yield self.i

    def start(self):
        self.runs=True
        self.event_source.start()

    def stop(self, event=None):
        self.runs = False
        self.event_source.stop()

    def forward(self, event=None):
        self.forwards = True
        self.start()
    def backward(self, event=None):
        self.forwards = False
        self.start()
    def oneforward(self, event=None):
        self.forwards = True
        self.onestep()
    def onebackward(self, event=None):
        self.forwards = False
        self.onestep()
    
    def get_slider_text(self):
        if self.i == 0:
            return "Initial"
        elif self.i >= self.simul.grow_start and self.i <= self.simul.grow_end:
            return "Growing {:3}".format(self.i - self.simul.grow_start + 1)
        elif self.i == self.simul.grow_end + 1:
            return "Post-Growing"
        elif self.i >= self.simul.secondary_elimination_start and self.i <= self.simul.secondary_elimination_end:
            return "Secondaries {:3}".format(self.i - self.simul.secondary_elimination_start + 1)
        elif self.i == self.simul.split_restart or self.i <= self.simul.split_end:
            index = self.i - self.simul.split_restart + 1 if self.i >= self.simul.split_restart else 0
            return "Splitting {:3}".format(index + 1)
        else:
            return "Final"
    
    def onestep(self):
        if self.i > self.slider_min and self.i < self.slider_max:
            self.i = self.i+self.forwards-(not self.forwards)
        elif self.i == self.slider_min and self.forwards:
            self.i+=1
        elif self.i == self.slider_max and not self.forwards:
            self.i-=1
        self.func(self.i)
        self.slider.set_val(self.i)
        self.slider.valtext.set_text(" {:<16}".format(self.get_slider_text()))
        self.fig.canvas.draw_idle()

    def setup(self):
        self.button_fig, self.button_ax = plt.subplots(num="Controls", figsize=(7,3))
        self.button_fig.canvas.mpl_connect('close_event', close_on_close)
        
        self.button_ax.set_axis_off()
        
        from mpl_toolkits.axes_grid1 import Divider
        from mpl_toolkits.axes_grid1.axes_size import Fixed, Scaled
        
        divider = Divider(self.button_fig, (0,0,1,1),
                          [
                            Scaled(0.15),
                            Scaled(1.),     #1
                            Scaled(0.05),
                            Fixed(0.5),     #3
                            Scaled(0.05),
                            Fixed(0.5),     #5
                            Scaled(0.05),
                            Fixed(0.5),     #7
                            Scaled(0.05),
                            Fixed(0.5),     #9
                            Scaled(0.05),
                            Fixed(0.5),     #11
                            Scaled(0.05),
                            Scaled(1.),     #13
                            Scaled(0.15)
                          ],
                          [
                            Scaled(0.05),
                            Scaled(1.),     #1
                            Scaled(0.05),
                            Fixed(1.5),     #3
                            Scaled(0.05),
                            Fixed(0.5),     #5
                            Scaled(0.05),
                            Fixed(0.5),     #7
                            Scaled(0.05)
                          ]
                         )
        
        self.button_forward = matplotlib.widgets.Button(self.button_fig.add_axes(divider.get_position(),
                                                                                 axes_locator=divider.new_locator(nx=7, ny=7)),
                                                        label='$\u25B6$')
                                                        
        self.button_back = matplotlib.widgets.Button(self.button_fig.add_axes(divider.get_position(),
                                                                                 axes_locator=divider.new_locator(nx=3, ny=7)),
                                                     label='$\u25C0$')
                                                     
        self.button_stop = matplotlib.widgets.Button(self.button_fig.add_axes(divider.get_position(),
                                                                                 axes_locator=divider.new_locator(nx=5, ny=7)),
                                                     label='| |')
                                                     
        self.button_oneforward = matplotlib.widgets.Button(self.button_fig.add_axes(divider.get_position(),
                                                                                 axes_locator=divider.new_locator(nx=11, ny=7)),
                                                           label='$\u29D0$')
                                                        
        self.button_oneback = matplotlib.widgets.Button(self.button_fig.add_axes(divider.get_position(),
                                                                                 axes_locator=divider.new_locator(nx=9, ny=7)),
                                                        label='$\u29CF$')
       
        self.button_forward.on_clicked(self.forward)
        self.button_back.on_clicked(self.backward)
        self.button_stop.on_clicked(self.stop)
        self.button_oneforward.on_clicked(self.oneforward)
        self.button_oneback.on_clicked(self.onebackward)
        
        self.slider = matplotlib.widgets.Slider(self.button_fig.add_axes(divider.get_position(),
                                                                         axes_locator=divider.new_locator(nx=2, nx1=12, ny=5)),
                                                'Step: ', self.slider_min, self.slider_max, valinit=self.i, valstep=1)
        self.slider.valtext.set_fontfamily('monospace')
        self.slider.on_changed(self.slider_callback)
        self.checkboxes = matplotlib.widgets.CheckButtons(self.button_fig.add_axes(divider.get_position(),
                                                                         axes_locator=divider.new_locator(nx=2, nx1=7, ny=3)),
                                                          ("Axis", "Cell Borders", "Cell Colours", "Secondaries"),
                                                          (True, True, True, False))
                                                          
        self.checkboxes.on_clicked(self.checkbox_callback)
        
        self.radioboxes = matplotlib.widgets.RadioButtons(self.button_fig.add_axes(divider.get_position(),
                                                                         axes_locator=divider.new_locator(nx=8, nx1=13, ny=3)),
                                                          ("None", "Tag", "Cluster", "SNR", "E", "Index"))
                                                          
        self.radioboxes.on_clicked(self.radiobutton_callback)
        
        
        #bot_ax = divider.append_axes("bottom", size=0.5, pad=0.1)
        #bot_divider = mpl_toolkits.axes_grid1.make_axes_locatable(bot_ax)
        #self.test1 = top_divider.append_axes("left", size=0.5, pad=0.05)
        #self.test2 = top_divider.append_axes("left", size=0.5, pad=0.05)
        #self.extra1 = matplotlib.widgets.Button(self.test1, label='A')
        #self.extra2 = matplotlib.widgets.Button(self.test2, label='B')
        
        #rbtnax = 
        #self.extra2 = matplotlib.widgets.Button(rbtnax, label='B')
        
    def slider_callback(self, _):
        self.i = self.slider.val
        self.slider.valtext.set_text(" {:<16}".format(self.get_slider_text()))
        self.func(self.i)
        self.fig.canvas.draw_idle()

    def checkbox_callback(self, label):
        self.simul.toggle_option(label)
        self.fig.canvas.draw_idle()

    def radiobutton_callback(self, label):
        self.simul.set_tag_style(label)
        self.fig.canvas.draw_idle()
        
    def update(self,i):
        self.slider.set_val(i)


namelist = []

simul = Simul([
                (0, 6, 1, 1, 1.273, 57.43, False),
                (1, 6, 1, 1, 0, 0.001, False),
                (2, 6, 1, 1, 1.841, 71.35, False),
                (5, 6, 1, 1, 1.913, 121.54, False),
                (6, 5, 2, 2, 2.214, 154.47, False),
                (8, 6, 1, 1, 0, 0.001, False),
                (11, 6, 1, 1, 2.116, 134.11, False),
                (12, 6, 1, 1, 10.117, 195.81, False),
                (13, 6, 1, 1, 3.415, 119.23, False),
                (1, 5, 1, 1, 1.957, 134.11, False),
                (2, 5, 1, 1, 1.571, 119.23, False),
                (3, 5, 1, 1, 1.947, 134.11, False),
                (4, 5, 1, 1, 1.742, 115.44, False),
                (5, 5, 1, 1, 2.012, 115.44, False),
                (8, 5, 1, 1, 1.987, 115.44, False),
                (9, 5, 1, 1, 2.430, 123.23, False),
                (10, 5, 1, 1, 3.101, 123.23, False),
                (11, 5, 1, 1, 2.541, 87.81, False),
                (12, 5, 1, 1, 3.571, 127.05, False),
                (0, 4, 1, 1, 0, 0.001, False),
                (1, 4, 1, 1, 3.672, 121.54, False),
                (2, 3, 1, 2, 2.893, 121.54, False),
                (3, 4, 2, 1, 0.971, 67.82, False),
                (5, 4, 1, 1, 3.444, 101.56, False),
                (6, 4, 1, 1, 6.321, 87.81, False),
                (7, 4, 1, 1, 3.194, 87.81, False),
                (8, 4, 1, 1, 1.812, 89.15, False),
                (9, 4, 2, 1, 1.141, 110.31, False),
                (11, 3, 1, 2, 3.577, 148.12, False),
                (12, 4, 1, 1, 3.112, 80.01, False),
                (13, 4, 1, 1, 1.994, 73.47, False),
                (0, 2, 1, 2, 0.547, 68.95, False),
                (1, 3, 1, 1, 5.124, 93.81, False),
                (3, 3, 2, 1, 0, 0.001, False),
                (5, 3, 1, 1, 1.214, 80.01, False),
                (6, 3, 1, 1, 2.225, 83.28, False),
                (7, 3, 1, 1, 3.141, 73.47, False),
                (8, 3, 1, 1, 3.214, 76.06, False),
                (9, 3, 2, 1, 1.891, 76.06, False),
                (12, 3, 1, 1, 2.453, 121.54, False),
                (13, 2, 1, 2, 3.261, 123.23, False),
                (1, 2, 1, 1, 3.444, 97.54, True),
                (2, 2, 1, 1, 3.261, 93.81, True),
                (3, 2, 1, 1, 1.820, 76.06, True),
                (4, 2, 1, 1, 0, 0.001, True),
                (5, 2, 1, 1, 1.742, 170.51, True),
                (6, 2, 1, 1, 1.489, 85.34, True),
                (7, 2, 1, 1, 3.584, 101.56, True),
                (8, 2, 1, 1, 4.215, 89.15, True),
                (9, 2, 1, 1, 3.147, 97.54, True),
                (10, 2, 1, 1, 1.845, 101.56, True),
                (11, 2, 1, 1, 2.219, 121.54, False),
                (12, 2, 1, 1, 2.715, 125.03, False),
                (1, 1, 1, 1, 1.101, 67.01, True),
                (2, 1, 1, 1, 2.042, 72.11, True),
                (3, 1, 1, 1, 0.745, 16.15, True),
                (4, 1, 1, 1, 0.905, 44.25, True),
                (5, 1, 1, 1, 4.108, 134.11, True),
                (6, 1, 1, 1, 1.078, 89.15, True),
                (7, 1, 1, 1, 2.121, 79.75, True),
                (8, 1, 1, 1, 3.741, 97.54, True),
                (9, 1, 1, 1, 2.812, 83.28, True),
                (10, 1, 1, 1, 1.945, 106.58, True),
                (11, 1, 1, 1, 2.216, 115.44, False),
                (12, 1, 1, 1, 0, 69.11, False),
                (1, 0, 2, 1, 1.011, 60.06, True),
                (5, 0, 2, 1, 0, 0.001, True),
                (7, 0, 2, 1, 1.491, 89.15, True),
                (11, 0, 2, 1, 3.991, 190.08, False)
              ],
              ["#ffd966","#a50021", "#2f5597", "#481f67", "#003300", "#fbe5d6", "#fff2cc", "#bfbfbf", "#99ffcc", "#93ffff", "#ffcc99", "#bdd7ee", "#d6dce5", "#f8cbad"],
              #mcolors.to_rgba_array(mcolors.XKCD_COLORS),
              ["A", "B", "C", "D", "E", "B1", "A1", "C1", "A2", "C2", "B3", "A3", "B4", "B2"])
              #[c1 + c2 for c1 in string.ascii_uppercase for c2 in string.ascii_uppercase])

plt.plot()

ani_grow = Player(simul)


plt.show()


#(1, 0, 2, 1, 0.995, 0.621, False),
#(3, 0, 2, 1, 1.210, 0.507, False),
#(0, 1, 1, 2, 0.713, 0.607, True),
#(1, 1, 1, 1, 2.231, 1.171, False),
#(2, 1, 2, 1, 5.121, 3.214, False),
#(4, 1, 1, 1, 0.000, 0.012, True),
#(5, 1, 1, 2, 1.354, 1.513, False),
#(1, 2, 2, 1, 2.374, 1.974, False),
#(3, 2, 2, 1, 2.485, 2.971, False),
#(0, 3, 2, 1, 3.321, 1.743, True),
#(2, 3, 1, 2, 3.147, 4.328, False),
#(3, 3, 1, 2, 2.911, 1.345, False),
#(4, 3, 2, 1, 3.741, 2.225, False),
#(0, 4, 1, 2, 4.321, 2.475, True),